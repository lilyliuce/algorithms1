#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 11 23:21:33 2017

@author: Lily Liu
"""

import numpy as np

def strassen_maxtrix_mul(A, B):
    '''
    numpy.matrix, numpy.matrix -> numpy.matrix
    Return the multiplication of two square matrices.
    An implementation of the Strassen’s Matrix Multiplication algorithm in Python.
    '''
    n = len(A)
    C = np.zeros((n, n))
    
    if n == 1:
        C[0,0] = A[0,0] * B[0,0]
    else:
        # Split the matrices into 4 sub-matrices.
        split_size = len(A) // 2
        
        A11, A12, A21, A22 = A[:split_size, :split_size], A[:split_size, split_size:], A[split_size:, :split_size], A[split_size:, split_size:]
                                                     
        B11, B12, B21, B22 = B[:split_size, :split_size], B[:split_size, split_size:], B[split_size:, :split_size], B[split_size:, split_size:]

        # Create matrices S1, S2, ... ,S10.
        S1 = B12 - B22
        S2 = A11 + A12
        S3 = A21 + A22
        S4 = B21 - B11
        S5 = A11 + A22
        S6 = B11 + B22
        S7 = A12 - A22
        S8 = B21 + B22
        S9 = A11 - A21
        S10 = B11 + B12
        
        # Recursively multiply matrices 7 times.
        P1 = strassen_maxtrix_mul(A11, S1)
        P2 = strassen_maxtrix_mul(S2, B22)
        P3 = strassen_maxtrix_mul(S3, B11)
        P4 = strassen_maxtrix_mul(A22, S4)
        P5 = strassen_maxtrix_mul(S5, S6)
        P6 = strassen_maxtrix_mul(S7, S8)
        P7 = strassen_maxtrix_mul(S9, S10)
        
        # Calculate C11, C12, C21, C22.
        C11 = P5 + P4 - P2 + P6
        C12 = P1 + P2
        C21 = P3 + P4
        C22 = P5 + P1 - P3 - P7
        
        # Grouping C11, C12, C21, C22 into matrix C.
        for i in range(0, split_size):
            for j in range(0, split_size):
                C[i][j] = C11[i][j]
                C[i][j + split_size] = C12[i][j]
                C[i + split_size][j] = C21[i][j]
                C[i + split_size][j + split_size] = C22[i][j]

    return C
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 29 01:01:19 2017

@author: Lily Liu
"""

def quick_sort(sort_list, p, r):
    '''
    Return the sorted list in the acsending order.
    An implementation of the heap sort algorithm in Python.
    '''
    if p < r :
        q = partition(sort_list, p, r)
        quick_sort(sort_list, p, q-1)
        quick_sort(sort_list, q + 1, r)
    
    return sort_list

def partition(sort_list, p, r):
    '''
    Return the rearranged subarry.
    '''
    last_elem = sort_list[r]
    i = p - 1
    for j in range(p, r):
        if sort_list[j] <= last_elem:
            i = i + 1
            sort_list[j], sort_list[i] = sort_list[i], sort_list[j]
    
    sort_list[i + 1], sort_list[r] = sort_list[r], sort_list[i + 1]
    
    return i + 1
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 11 23:19:38 2017

@author: Lily Liu
"""

def insert_sort(sort_list):
    '''
    list -> list
    Return the sorted list in the acsending order.
    An implementation of the insertion sort algorithm in Python.
    '''
    for i in range(1, len(sort_list)):
        key = sort_list[i]
        
        j = i - 1
        while j >= 0 and sort_list[j] > key:
            sort_list[j + 1] = sort_list[j]
            j = j - 1
          
        sort_list[j + 1] = key
                   
    return sort_list
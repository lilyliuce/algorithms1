#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 19 23:29:02 2017

@author: lilyliuce
"""

def max_heapify(sort_list, parent_index):
    '''
    list, index -> list
    Return the list whose sub-tree obeys the max-heap property.
    '''
    
    # Get the indexes of left node of right node.
    left_index = 2 * parent_index + 1
    right_index = 2 * (parent_index + 1)
    
    largest_index = parent_index
    
    # Compare the left node with the parent node.
    if left_index < len(sort_list) and sort_list[left_index] > sort_list[parent_index]:
        largest_index = left_index
        
    # Compare the right node with the parent node.
    if right_index < len(sort_list) and sort_list[right_index] > sort_list[largest_index]:
        largest_index = right_index
        
    if largest_index != parent_index:
        temp = sort_list[parent_index]
        sort_list[parent_index] = sort_list[largest_index]
        sort_list[largest_index] = temp
        max_heapify(sort_list, largest_index)
        
    return sort_list

def build_max_heap(sort_list):
    '''
    list -> list
    Return the list whose every parent node in the tree obeys the max-heap property.
    '''
    last_parent_node = (len(sort_list) - 1) // 2

    for i in range(last_parent_node, -1, -1):
        max_heapify(sort_list, i)
        
    return sort_list

def heap_sort(sort_list):
    '''
    list -> list
    Return the sorted list in the acsending order.
    An implementation of the heap sort algorithm in Python. 
    '''
    # Create a list to save the ordered list.
    ordered_list = [None] * (len(sort_list))
    sort_list = build_max_heap(sort_list)

    for i in range(len(sort_list) - 1, 0, -1):
        # Swap the first element with the last element.
        temp = sort_list[0]
        sort_list[0] = sort_list[i]
        ordered_list[i] = temp
 
        sort_list.pop()
        max_heapify(sort_list, 0)
    
    ordered_list[0] = sort_list[0]
    
    return ordered_list
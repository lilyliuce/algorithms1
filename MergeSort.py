#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 11 23:20:33 2017

@author: Lily Liu
"""

def merge_sort(sort_list, start_index, end_index):
    '''
    list, start_index, end_index -> list
    Return the sorted list in the acsending order.
    An implementation of the merge sort algorithm in Python.
    '''
    if start_index < end_index:
        # Split sort_list into two sub-list
        mid_index = (start_index + end_index) // 2
        merge_sort(sort_list, start_index, mid_index)
        merge_sort(sort_list, mid_index + 1, end_index)
        merge(sort_list, start_index, mid_index, end_index)
        
    return sort_list

def merge(sort_list, start_index, mid_index, end_index):
    '''
    list, start_index, mid_index, end_index -> list
    Return the merged list in the acsending order 
        between index start_index and end_index.
    '''

    # Create two new lists with the sorted elements in the sort_list.
    first_list = sort_list[start_index : (mid_index + 1)]
    sec_list = sort_list[(mid_index + 1) : (end_index + 1)]
    
    # Set the sentinel with float('inf').
    first_list.append(float('inf'))
    sec_list.append(float('inf'))
    
    # Merge the two lists.
    j = 0
    k = 0
    for i in range(start_index, end_index + 1):
        if first_list[j] > sec_list[k]:
            sort_list[i] = sec_list[k]
            k = k + 1
        else:
            sort_list[i] = first_list[j]
            j = j + 1
            
    return sort_list